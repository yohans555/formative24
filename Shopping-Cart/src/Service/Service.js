import axios from 'axios'

const api_url_get = 'http://localhost:8080/';

class Service {
    
    async getData(target){
        const url_target = api_url_get+"get/"+target
        return  axios.get(url_target);
    }

    saveData(target,jsondata){
        const url_target = api_url_get+"save/"+target
        return  axios.post(url_target,jsondata)
    }

}

export default new Service()
