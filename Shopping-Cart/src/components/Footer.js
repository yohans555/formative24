import React from 'react';
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { addToCart } from './actions/cartActions'



 const Footer = ()=>{

    return(
            <div id='footer'>
              <div class="background">
                <div class="body">
                 <div class="subscribe">
                  <h3>Get Weekly Newsletter</h3>
                  <form>
                  <input type="text" value="" class="txtfield"></input>
                  <input type="text" value="" class="button"></input>
                  </form>
                 </div>
                 <div class="posts">
                  <h3>Latest Post</h3>
                  <p>Lorem Ipsum DOlor Sit Amet</p>
                 </div>
                 <div class="connect">
                  <h3>Follow Us:</h3>
                  <a class='facebook'></a><a class='twitter'></a><a class='googleplus'></a>
                 </div>
                </div>
              </div>
              <span id="footnote"> <a>Moonstrosity Custom Shirts</a> &copy; 2012 | All Rights Reserved.</span>
            </div>
    )
}

  export default Footer