import React, { Component } from 'react';
import { connect } from 'react-redux'
import { addToCart } from './actions/cartActions'
import Service from '../Service/Service'
import Item1 from '../images/item1.jpg'
import Item2 from '../images/item2.jpg'
import Item3 from '../images/item3.jpg'


 class Home extends Component{
    
    handleClick = (id)=>{
        this.props.addToCart(id); 
    }
    componentDidMount(){
        Service.getData("product").then((response)=>{
            this.setState({items:response.data})  
        })
    }

    render(){
            return(
                <div id='body'>
                <div id='contents'>
                    <h1>Men</h1>
                    <ul id="shirts">
                    {
                        this.props.items.map(product =>(
                            <li  key={product.id}>
                            <a><img src={product.img}></img></a><a className='button' onClick={()=>{this.handleClick(product.id)}}>Add to Cart</a>
                            <p>
                                {product.title} <br></br><span>&#36;{product.price}</span>
                            </p>
                            </li>
                        ))

                    }
                        
                    </ul>
                </div>
                </div>
            )
    }
}
const mapStateToProps = (state)=>{
    return {    
      items: state.items,
    }
  }
const mapDispatchToProps= (dispatch)=>{
    
    return{
        addToCart: (id)=>{dispatch(addToCart(id))}
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(Home)