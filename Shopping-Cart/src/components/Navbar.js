import React from 'react';
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { addToCart } from './actions/cartActions'
import { render } from 'react-dom';
import  { Component } from 'react';



class Navbar extends Component{
  render(){
    return(
      <div id="header">
<div>
 <div id="navigation">
   <div class="infos">
     <a><Link to='/cart'>Cart</Link></a> <a href="/cart">{this.props.items} items</a>
   </div>
   <div>
     <a>Login</a> <a href="">Register</a>
   </div>
   <ul id="primary">
     <li>
       <a href="">
         <span>Home</span>
       </a>
     </li>
     <li>
       <a href="">
         <span>About</span>
       </a>
     </li>
     <li class="selected">
       <a href="/">
         <span>Men</span>
       </a>
     </li>
   </ul>
   <ul id="secondary">
     <li>
       <a href="">
         <span>Women</span>
       </a>
     </li>
     <li>
       <a href="">
         <span>Blog</span>
       </a>
     </li>
     <li>
       <a href="">
         <span>Contact</span>
       </a>
     </li>
   </ul>
   <a id='logo'><img src="images/logo.png"></img></a>
   </div>
 </div>
</div>
)
  }
}

const mapStateToProps = (state)=>{
    let qty = 0
    let list= state.addedItems

    list.forEach(element => {
        qty = qty + element.quantity
    });
    return {
      items: qty,
    }
  }

const mapDispatchToProps= (dispatch)=>{
    return{
        addToCart: (id)=>{dispatch(addToCart(id))}
    }
}

  export default connect(mapStateToProps,mapDispatchToProps)(Navbar)