package com.nexsoft.app.Controller;

import java.util.ArrayList;
import java.util.List;

import com.nexsoft.app.model.Model;
import com.nexsoft.app.model.Product;
import com.nexsoft.app.repo.Productrepository;
import com.nexsoft.app.service.Modelservice;
import com.nexsoft.app.service.Productservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class Modelcontroller {
    @Autowired
    Modelservice modelservice;

    @Autowired
    Productservice productservice;

    @CrossOrigin(origins = "http://localhost:3000")
    @GetMapping(value ="/get/product", produces = "application/json")
    public ArrayList<Product> getData(){
        return productservice.get();
    }

    
    @CrossOrigin(origins = "http://localhost:3000")
    @RequestMapping(value ="/save/recipe", method = RequestMethod.POST)
    public void add(@RequestBody ArrayList<Model> model ){
        modelservice.saveall(model);
    }
}
