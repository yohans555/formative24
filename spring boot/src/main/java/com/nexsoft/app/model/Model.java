package com.nexsoft.app.model;

import java.io.Serializable;

import javax.persistence.*;



@Entity
@Table(name="modalsu")
public class Model implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String title,desc,img;
    private int price,quantity;

    public Model(){}

    

    public Model(int id, String title, String desc, String img, int price, int quantity) {
        this.id = id;
        this.title = title;
        this.desc = desc;
        this.img = img;
        this.price = price;
        this.quantity = quantity;
    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    

}
