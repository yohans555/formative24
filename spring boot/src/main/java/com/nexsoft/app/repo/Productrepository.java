package com.nexsoft.app.repo;

import java.util.ArrayList;

import com.nexsoft.app.model.Model;
import com.nexsoft.app.model.Product;

import org.springframework.data.jpa.repository.JpaRepository;
// import org.springframework.data.repository.CrudRepository;

public interface Productrepository extends JpaRepository<Product, Integer> {
}


