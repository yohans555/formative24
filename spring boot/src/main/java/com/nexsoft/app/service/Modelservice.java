package com.nexsoft.app.service;

import java.util.ArrayList;

import javax.transaction.Transactional;

import com.nexsoft.app.model.Model;
import com.nexsoft.app.repo.Modelrepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class Modelservice {
    @Autowired
     private Modelrepository modelrepository;

    @Transactional
    public ArrayList<Model> get(){
        return (ArrayList<Model>) modelrepository.findAll();
    }
    @Transactional
	public void saveall(ArrayList<Model> model) {
		modelrepository.saveAll(model);
	}

    @Transactional
	public void save(Model model) {
		modelrepository.save(model);
	}
    
}
