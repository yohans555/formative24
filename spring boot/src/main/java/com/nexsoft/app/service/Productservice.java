package com.nexsoft.app.service;

import java.util.ArrayList;

import javax.transaction.Transactional;

import com.nexsoft.app.model.Model;
import com.nexsoft.app.model.Product;
import com.nexsoft.app.repo.Modelrepository;
import com.nexsoft.app.repo.Productrepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class Productservice {
    @Autowired
     private Productrepository productrepository;

    @Transactional
    public ArrayList<Product> get(){
        return (ArrayList<Product>) productrepository.findAll();
    }
}
